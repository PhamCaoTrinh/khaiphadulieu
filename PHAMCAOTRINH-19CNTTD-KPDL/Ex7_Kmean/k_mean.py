from cProfile import label
from dis import dis
from turtle import color, distance
import matplotlib
from matplotlib.pyplot import xscale
import numpy as np
from sklearn import cluster
import matplotlib.pyplot as plt

np.random.seed(42)

def euclidean_distance(x1,x2):
  return np.sqrt(np.sum(x1-x2)**2)

class KMeans:

  def __init__(self, K=5, max_iters=100, plot_steps=False):
    self.K = K
    self.max_iters = max_iters
    self.plot_steps = plot_steps

    #list of sample indices for each cluster
    self.clusters = [[] for _ in range(self.K)]
    #mean feature vector for each cluster
    self.centroids = []

  def predict(self, X):
    self.X = X
    self.n_samples, self.n_features = X.shape

    #initalize centroids
    random_sample_idxs = np.random.choice(self.n_samples, self.K, replace=False)
    self.centroids = [self.X[idx] for idx in random_sample_idxs]
    
    #opzimization
    for _ in range(self.max_iters):

      #update clusters
      self.clusters = self._create_clusters(self.centroids)
      if self.plot_steps:
        self.plot()
      #update centroids
      centroids_old = self.centroids
      self.centroids = self._get_centroids(self.clusters)
      if self.plot_steps:
        self.plot()
      #check if converged
      if self._is_converged(centroids_old, self.centroids):
        break
    
    return self._get_cluster_labels(self.clusters)

  def _get_cluster_labels(self, clusters):
    labels = np.empty(self.n_samples)
    for cluster_idx, cluster in enumerate(clusters):
      for sample_idx in cluster:
         labels[sample_idx] = cluster_idx
    return labels

  def _create_clusters(self, centroids):
    clusters = [[] for _ in range(self.K)]
    for idx, sample in enumerate(self.X):
      centroids_idx = self._closet_centroid(sample, centroids)
      clusters[centroids_idx].append(idx)
    return clusters

  def _closet_centroid(self, sample, centroids):
    distances = [euclidean_distance(sample, point) for point in centroids]
    closet_idx = np.argmin(distances)
    return closet_idx

  def _get_centroids(self, clusters):
    centroids = np.zeros((self.K, self.n_features))
    for cluster_idx, cluster in enumerate(clusters):
      cluster_mean = np.mean(self.X[cluster], axis=0)
      centroids[cluster_idx] = cluster_mean
    return centroids

  def _is_converged(self, centroids_old, centroids):
    distances = [euclidean_distance(centroids_old[i], centroids[i]) for i in range(self.K)]
    return sum(distances) == 0

  def plot(self):
    x = [2,2,8,5,7,6,1,4]
    y = [10,5,4,8,5,4,2,9]
    label = [0,1,2,3,4,5,6,7]
    colors = ['red','green','blue','purple','yellow','pink','black','orange']

    fig = plt.figure()
    plt.scatter(x, y, c=label, cmap=matplotlib.colors.ListedColormap(colors))
    
    plt.show()