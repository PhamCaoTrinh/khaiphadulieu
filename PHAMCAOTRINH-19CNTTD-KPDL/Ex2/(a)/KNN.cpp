#include <bits/stdc++.h>
#include <fstream>
#include <iostream>
using namespace std;
struct Point{
    char val; 
    double x, y;
    double distance;
};

char phanLopKnn(Point arr[], int n, int k, Point p)
{
    for(int i =0 ;i <n ;i++)
    {
        arr[i].distance = sqrt(((arr[i].x - p.x)*(arr[i].x - p.x)) + ((arr[i].y - p.y)*(arr[i].y - p.y))  );
    }
    

    for(int i=0;i<n-1;i++)
    {
        for(int j = i+1; j<n;j++)
        {
            if(arr[i].distance  > arr[j].distance)
            {
                swap(arr[i], arr[j]);
            }
        }
    }
    int dem1 = 0;
    int dem2 = 0;
    for(int i=0;i<k;i++)
    {
        if(arr[i].val == '-')
        {
            dem1++;
        }
        else if(arr[i].val == '+')
        {
            dem2++;
        }
    }
    if(dem1 > dem2)
    {
        return '-';
    }
    else{
        return '+';
    }
}

int main()
{
    int n;
    Point arr[100];
	ifstream ifs("training.txt");
	ifstream ifs1("testting.txt");
	ifs>>n;
	for(int i=0;i<n;i++){
		ifs>>arr[i].x>>arr[i].y>>arr[i].val;
	}
    Point p;
    ifs1>>p.x>>p.y;
    char nhom = phanLopKnn(arr, n, 7, p);
    ofstream ofs("output.txt");
    cout<<"Class : ";
    cout<<p.x<<" "<<p.y<<" "<<nhom<<endl;
    ofs<<"Class : ";
    ofs<<nhom;
    ofs.close();
    system("pause");
    return 0;
}
