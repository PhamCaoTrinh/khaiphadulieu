#include <bits/stdc++.h>
#include <fstream>
#include <vector>

//Outlook Sunny: 1, Overcast: 2, Rainy: 3
//Temp Hot: 1, Mild: 2, Cool: 3
//Humidity High: 1, Normal: 2
//Windy True: 1, False: 2
//Play Yes: 1, No: 0


using namespace std;
struct Point{
    int val; // class cua point
    double x, y, z, w;
    double distance;
};

int main()
{	
	//read train file
	ifstream trainfile("D:/Khai pha du lieu/C++/Bayes Code By C++/Ex5/training_dataset.txt");
	vector<string> lines;
	string line;
	int n;
    Point arr[10000];
    int sum_train = 0;
    double sum_pos = 0, sum_neg = 0; 
	
    while (getline(trainfile, line, ' ')){
        lines.push_back(line);
		sum_train++;
    }
    sum_train/=5;
    for(int y = 0 ; y < sum_train ; y++){
	    	for (const auto &i : lines){
				arr[y].x = std::stoi(i);
				lines.erase(lines.begin());
				break;
			}
			for (const auto &i : lines){
				arr[y].y = std::stoi(i);
				lines.erase(lines.begin());
				break;
			}
			for (const auto &i : lines){
				arr[y].z = std::stoi(i);
				lines.erase(lines.begin());
				break;
			}
			for (const auto &i : lines){
				arr[y].w = std::stoi(i);
				lines.erase(lines.begin());
				break;
			}
			for (const auto &i : lines){
				arr[y].val = std::stoi(i);
				if(arr[y].val == 1){
					sum_pos++;
				} else sum_neg++;
				lines.erase(lines.begin());
				break;
			}
		}
		
	//read test file
	ifstream testfile("D:/Khai pha du lieu/C++/Bayes Code By C++/Ex5/testing_dataset.txt");
    Point p;
    vector<string> lines_test;
	string line_test;
	int sum_test=1;
	double att1_pos = 0, att1_neg = 0;
	double att2_pos = 0, att2_neg = 0;
	double att3_pos = 0, att3_neg = 0;
	double att4_pos = 0, att4_neg = 0;
	
    while (getline(testfile, line_test, ' ')){
        lines_test.push_back(line_test);
		sum_test++;  
    }
    sum_test/=5;
	cout<<"Bayes code by C++"<<endl;
	cout<<endl;
    cout<<"Predict label for a sample with: "<<endl;
    for(int y = 0 ; y < sum_test ; y++){
	    	for (const auto &i : lines_test){
				p.x = std::stoi(i);
				cout<<"x: "<<p.x<<endl;
				lines_test.erase(lines_test.begin());
				break;
			}
			for (const auto &i : lines_test){
				p.y = std::stoi(i);
				cout<<"y: "<<p.y<<endl;
				lines_test.erase(lines_test.begin());
				break;
			}
			for (const auto &i : lines_test){
				p.z = std::stoi(i);
				cout<<"z: "<<p.z<<endl;
				lines_test.erase(lines_test.begin());
				break;
			}
			for (const auto &i : lines_test){
				p.w = std::stoi(i);
				cout<<"w: "<<p.w<<endl;
				lines_test.erase(lines_test.begin());
				break;
			}
		}
		
	//calculate P
	for(int i = 0; i < sum_train ; i++){
		if(p.x == arr[i].x){
			if(arr[i].val == 1){
				att1_pos++;
			}
			else{
				att1_neg++;
			}
		}
		if(p.y == arr[i].y){
			if(arr[i].val == 1){
				att2_pos++;
			}
			else{
				att2_neg++;
			}
		}
		if(p.z == arr[i].z){
			if(arr[i].val == 1){
				att3_pos++;
			}
			else{
				att3_neg++;
			}
		}
		if(p.w == arr[i].w){
			if(arr[i].val == 1){
				att4_pos++;
			}
			else{
				att4_neg++;
			}
		}
	}
	
	
	double P1 = (att1_pos/sum_pos) * (att2_pos/sum_pos) * (att3_pos/sum_pos) * (att4_pos/sum_pos) * (sum_pos/sum_train);
	double P2 = (att1_neg/sum_neg) * (att2_neg/sum_neg) * (att3_neg/sum_neg) * (att4_neg/sum_neg) * (sum_neg/sum_train);
	
	cout<<endl;
	if(P1 > P2){
		cout<<"P(X | YES)["<<P1<<"] > P(X | NO)["<<P2<<"]"<<endl;
		cout<<"Predict label: 1"<<endl;
		cout<<"Conclusion -> YES"<<endl;
	}
	else{
		cout<<"P(X | YES)["<<P1<<"] < P(X | NO)["<<P2<<"]"<<endl;
		cout<<"Predict label: 0"<<endl;
		cout<<"Conclusion -> NO"<<endl;
	}
	
    return 0;
}
