from math import sqrt

# Euclidean distance
def euclidean_distance(row1, row2):
	distance = 0.0
	for i in range(len(row1)):
		distance += (row1[i] - row2[i])**2
	return sqrt(distance)

# Get neighbors
def get_neighbors(train, test_row, num_neighbors):
	distances = list()
	for train_row in train:
		dist = euclidean_distance(test_row, train_row)
		distances.append((train_row, dist))
	distances.sort(key=lambda tup: tup[1])
	neighbors = list()
	for i in range(num_neighbors):
		neighbors.append(distances[i][0])
	return neighbors

# Prediction
def predict_classification(train, test_row, num_neighbors):
	neighbors = get_neighbors(train, test_row, num_neighbors)
	output_values = [row[-1] for row in neighbors]
	prediction = max(set(output_values), key=output_values.count)

	return prediction


# Test distance function
f = open('../../THUATTOAN/BaiTapKPDL/kteam.txt')

datasetTemp = f.readline()
f.close()
datasetTemp = datasetTemp.split('],[')

#tách nó thành list có các chuỗi list

# dataset = list(map(list,data.split('],[')))

dataset = list()
for i in datasetTemp:
	i=list(map(int,i.split(',')))

	# biến từng list bị phong ấn dưới dạng chuỗi thành list
	# gỡ từng con số split(',') => biến đổi sang int và gộp thành list
	# print(i)

	dataset.append(i)

att1 = float(input("Attribute 1: "))
att2 = float(input("Attribute 2: "))
test = [att1,att2]

k = int(input("Select k-neighbors: "))

prediction = predict_classification(dataset, test, k)

print("Training dataset: ", dataset)
print("Testing data: ", test)

if prediction == 1:
	print("==> Label of testing data is (+).")
else:
	print("==> Label of testing data is (-).")


