import csv
import numpy as np
import math
from sklearn.metrics import confusion_matrix

def loadData(path):
    f = open(path, "r")
    data = csv.reader(f) #csv format
    data = np.array(list(data))# covert to matrix
    data = np.delete(data, 0, 0)# delete header
    data = np.delete(data, 0, 1) # delete index
    np.random.shuffle(data) # shuffle data
    f.close()
    trainSet = data[:16] #training data from 1->16
    testSet = data[16:]# the others is testing data
    return trainSet, testSet
# tính khoảng cách
def calcDistancs(pointA, pointB, numOfFeature=2):
    tmp = 0
    for i in range(numOfFeature):
        tmp += (float(pointA[i]) - float(pointB[i])) ** 2
    return math.sqrt(tmp)

# tìm k dữ liệu gần nhất
def kNearestNeighbor(trainSet, point, k):
    distances = []
    for item in trainSet:
        distances.append({
            "label": item[-1],
            "value": calcDistancs(item, point)
        })
    distances.sort(key=lambda x: x["value"])
    labels = [item["label"] for item in distances]
    return labels[:k]

#tìm kiếm nhãn vừa tìm được
def findMostOccur(arr):
    labels = set(arr) # set label
    ans = ""
    maxOccur = 0
    for label in labels:
        number = arr.count(label)
        if number > maxOccur:
            maxOccur = number
            ans = label
    return ans
#Duyệt qua các giá trị trong bộ dữ liệu test để kiểm tra
trainSet, testSet = loadData("./KNN_dataset.csv")
numOfRightAnwser = 0
labels = []
predicted = []
for item in testSet:
    knn = kNearestNeighbor(trainSet, item, 5)
    answer = findMostOccur(knn)
    numOfRightAnwser += item[-1] == answer
    print("X1: {}, X2: {}, label: {} -> predicted: {}".format(item[0], item[1], item[-1], answer))
    labels.append(item[-1])
    predicted.append(answer)

print("\nGround truth labels:" , labels)
print("Predicted labels:" , predicted)
conf_mat = confusion_matrix(labels, predicted)
print("\nConfusion matrix:")
print(conf_mat)
print("\nAccuracy", numOfRightAnwser / len(testSet))

f = open("./KNN_accuracy.csv", mode = 'w', encoding = 'utf-8')
f.write("\nGround truth labels: {}".format(labels))
f.write("\nPredicted labels: {}".format(predicted))
f.write("\nConfusion matrix:")
f.write("\n{}".format(conf_mat))
f.write("\nAccuracy {}".format(numOfRightAnwser / len(testSet)))
f.close()
