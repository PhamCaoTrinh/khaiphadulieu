
#include <bits/stdc++.h>

using namespace std;
struct Point{
    string val; 
    double x;
    double y;
    double z;
    double f;
    double distance;
};

//KNN
void phanLopKnn(Point arr[], int n, Point p)
{
	cout<<"\n";
    for(int i =0 ;i <n ;i++)
    {
        arr[i].distance = sqrt(	((arr[i].x - p.x)*(arr[i].x - p.x))
								+((arr[i].y - p.y)*(arr[i].y - p.y)) 
								+((arr[i].z - p.z)*(arr[i].z - p.z))
								+((arr[i].f - p.f)*(arr[i].f - p.f)));
    }
    for(int i=0;i<n-1;i++)
    {
        for(int j = i+1; j<n;j++)
        {
            if(arr[i].distance  > arr[j].distance)
            {
                swap(arr[i], arr[j]);
            }
        }
    }
}
//ket qua label
string ketqua(Point arr[], int k){
    double dem1 = 0;
    double dem2 = 0;
    double dem3 = 0;
    int ins = 0;
    string x ;
    string y ;
    string z ;
    string index1 = arr[0].val; 
    string index2 ;
    string index3 ;

    for(int i=0; i<k; i++)
    {
        if(arr[i].val != index1)
        {
        index2 = arr[i].val; 
          continue;
          
        }
        else if(arr[i].val != index1 && arr[i].val != index2)
        {
            index3 = arr[i].val;  
     		break;        
        }
        
    }
    
    for(int i=0; i<k; i++)
    {
    	if(arr[i].val == index1)
        {
            dem1++;
            x = index1;
          
        }
        else if(arr[i].val == index2)
        {
        	 dem2++;
            y = index2;
		}
        else 
        {         
            dem3++;
            z = index2;
        }
	}

    if(dem1 > dem2)
    {
        if(dem1>dem3)
        {
        	return x;
		}else
		{
			return z;
		}
		
    }	
    else{
        if(dem2>dem3)
        {
        	return y;
			
		}else
		{
			return z;
		}
    }
}


//xao mang
void Swap(Point* number_1, Point* number_2)
{
	Point temp = *number_1;
	*number_1 = *number_2;
	*number_2 = temp;
}

void ramdom_data(Point* arr, int n)
{
	srand(time(NULL));

	int minPosition;
	int maxPosition = n - 1;
	int swapPosition;

	int i = 0;
	while (i < n - 1)
	{
		minPosition = i + 1;
		swapPosition = rand() % (maxPosition - minPosition + 1) + minPosition;

		Swap(&arr[i], &arr[swapPosition]);
		i++;
	}
}
//xuat
void Display(int n, Point arr[]){
	for(int i=0; i<n; i++){

		cout<<"  "<<arr[i].x<<"  "<<arr[i].y<<"  "<<arr[i].val<<endl;
	}
}
//danh gia
void Confusion_Matrix(int n,int m, Point p[], Point arr[]){

	
    int	str1s = 0;
    int	str2s = 0;
    int str3s = 0;
    
    int	str1st = 0;
    int	str2st = 0;
    int str3st = 0;
    
    int	str1sts = 0;
    int	str2sts = 0;
    int str3sts = 0;
    
	
	string index1 = "100"; 
    string index2 = "010";
    string index3 = "001";
	


    
	for(int i=0; i<n-m; i++){
		  if(p[i].val==index1 && p[i].val==arr[i].val )
		    {
		    	
               str1s++;
            }
    		   	
         else if(p[i].val == index2 && p[i].val==arr[i].val )
	      {

    		   	str2s++;
		  }
		  else if(p[i].val == index3 && p[i].val==arr[i].val )
	      {
                str3s++;
	      }
		}

		for(int i=0; i<n-m; i++){
		  if(p[i].val == index1 && p[i].val!=arr[i].val )
		    {
		    	
    		   if(index2 == arr[i].val)
    		   {
    		   	    str1st++;
    		   	     continue;
    		   	  
    		   }else if( index3 == arr[i].val )
    		    {
    		    	str1sts++;
    		    	 continue;
    		   	}
    	   }
         else if(p[i].val == index2 && p[i].val!=arr[i].val )
	      {
	     	    if(index2 == arr[i].val)
    		   {
    		   	  str2st++;
    		   	   continue;
    		   	  
    		   }else if( index3 == arr[i].val )
    		    {
    		    	str2sts++;
    		    	 continue;
    		   	}
    		   	
		  }
		  else if(p[i].val == index3 && p[i].val!=arr[i].val )
	      {
	     	    if(index2 == arr[i].val)
    		   {
    		   	  str3st++;
    		   	   continue;
    		   	  
    		   }else if( index3 == arr[i].val )
    		    {
    		    	str3sts++;
    		    	 continue;
    		   	}
		}
		
		}

		cout<<"Ma tran: "<<endl;
		cout<<str1s<< " " <<str1st<<" "<<str1sts<<endl;
		cout<<str2st<< " " <<str2s<<" "<<str2sts<<endl;
		cout<<str3st<< " " <<str3sts<<" "<<str3s<<endl;
        cout<<"\nti le chinh xac : "<<str1s+str2s+str3s<<"/"<<n-m<<" = "<<(double)100*(str1s+str2s+str3s)/(n-m)<<"%";
}


int main(){
	int k,d,f;
	string label;
    int n;
    Point arr[1000];
    Point p[1000];
	ifstream ifsi("init.txt");
	ifsi>>d;
	ifsi>>f;
	ifstream ifs("training_data.txt");
	ifs>>n;
	
	for(int i=0;i<n;i++){
		ifs>>arr[i].x>>arr[i].y>>arr[i].z>>arr[i].f>>arr[i].val;
	}
	
		
		for(int i=0;i<n;i++){
		 cout<<arr[i].f<<"0, ";
	}

    
	ramdom_data(arr,n);

	int m;
	m=n*8/10;	//80%

	ofstream ofs("ground_truth.txt");
	for(int i=n-1; i>m-1; i--){

		ofs<<arr[i].x<<"  "<<arr[i].y<<"  "<<arr[i].z<<"  "<<arr[i].f<<"  "<<arr[i].val<<endl;
	}
		//20%
	ofstream ofs1("testting_data.txt");
	for(int i=n-1; i>m-1; i--){

		ofs1<<arr[i].x<<"  "<<arr[i].y<<"  "<<arr[i].z<<"  "<<arr[i].f<<endl;
	}
	ofs.close();
		ifstream ifs1("testting_data.txt");
		for(int i=0;i<n-m;i++){
		 ifs1>>p[i].x>>p[i].y>>p[i].z>>p[i].f;
	}
	ifs1.close();
	

	
	cout<<"data testting:\n";
	    	for(int i=0;i<n-m;i++){
			cout<<p[i].x<<"  "<<p[i].y<<"  "<<"  "<<p[i].z<<"  "<<p[i].f<<endl;
	}
	ofstream ofs2("output.txt");
    for(int i=0; i<n-m; i++){
   		phanLopKnn(arr, m, p[i]);
   		string nhom = ketqua(arr, f);	
   		p[i].val=nhom;
   		ofs2<<"x1"<<"="<<p[i].x<<"	x2"<<"="<<p[i].y<<" x3"<<"="<<p[i].z<<"	x4"<<"="<<p[i].f<<"	==> y= "<<nhom<<endl;
	};
	Confusion_Matrix(n,m,p,arr);
	
    return 0;
    
}
