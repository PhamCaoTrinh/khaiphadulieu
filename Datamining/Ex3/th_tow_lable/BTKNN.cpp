
#include <bits/stdc++.h>

using namespace std;
struct Point{
    string val; 
    double x;
    double y;
    double distance;
};

//KNN
void phanLopKnn(Point arr[], int n, Point p)
{
	cout<<"\n";
    for(int i =0 ;i <n ;i++)
    {
        arr[i].distance = sqrt(	((arr[i].x - p.x)*(arr[i].x - p.x))
								+((arr[i].y - p.y)*(arr[i].y - p.y)) );
    }
    for(int i=0;i<n-1;i++)
    {
        for(int j = i+1; j<n;j++)
        {
            if(arr[i].distance  > arr[j].distance)
            {
                swap(arr[i], arr[j]);
            }
        }
    }
}
//ket qua label
string ketqua(Point arr[], int k){
    double dem1 = 0;
    double dem2 = 0;
    string x ;
    string y ;
    string index = arr[0].val;
    for(int i=0;i<k;i++)
    {
        if(arr[i].val == index)
        {
            dem1++;
            x = index;
          
        }
        else if(arr[i].val != index)
        {
            dem2++;
            y = arr[i].val;
            
        }
    }
    if(dem1 > dem2)
    {
        return x;
    }	
    else{
        return y;
    }
}


//xao mang
void Swap(Point* number_1, Point* number_2)
{
	Point temp = *number_1;
	*number_1 = *number_2;
	*number_2 = temp;
}

void ramdom_data(Point* arr, int n)
{
	srand(time(NULL));

	int minPosition;
	int maxPosition = n - 1;
	int swapPosition;

	int i = 0;
	while (i < n - 1)
	{
		minPosition = i + 1;
		swapPosition = rand() % (maxPosition - minPosition + 1) + minPosition;

		Swap(&arr[i], &arr[swapPosition]);
		i++;
	}
}
//xuat
void Display(int n, Point arr[]){
	for(int i=0; i<n; i++){

		cout<<"  "<<arr[i].x<<"  "<<arr[i].y<<"  "<<arr[i].val<<endl;
	}
}
//danh gia
void Confusion_Matrix(int n, int m, Point p[], Point arr[]){
	int str1 = 0;
	int str2 = 0;
    int	str1s = 0;
    int	str2s = 0;
	int kiemtra;
	string index = p[0].val;
	
		for(int i=0; i<n-m; i++){
		  if(p[i].val==index){
    		str1++;
		}else
		{
	     	str2++;
		}
		}
		for(int i=0; i<n-m; i++){
		  if(p[i].val==index && p[i].val==arr[i].val )
		    {
    		str1s++;
    		}
          if(p[i].val!=index && p[i].val==arr[i].val )
	      {
	     	str2s++;
		  }
		}
		cout<<"Ma tran: "<<endl;
		cout<<str1s<< " " <<str2 - str2s<<endl;
		cout<<str1 - str1s<< " " <<str2s;
    cout<<"\nti le chinh xac : "<<str1s+str2s<<"/"<<n-m<<" = "<<(double)100*(str1s+str2s)/(n-m)<<"%";
}


int main(){
	int k,d,f;
	string label;
    int n;
    Point arr[100];
    Point p[100];
	ifstream ifsi("init.txt");
	ifsi>>d;
	ifsi>>f;
	ifstream ifs("training_data.txt");
	ifs>>n;
	for(int i=0;i<n;i++){
		ifs>>arr[i].x>>arr[i].y>>arr[i].val;
	}
    
	ramdom_data(arr,n);

	int m;
	m=n*8/10;	//80%

	ofstream ofs("training_lable.txt");
	for(int i=n-1; i>m-1; i--){

		ofs<<arr[i].x<<"  "<<arr[i].y<<"  "<<arr[i].val<<endl;
	}
		//20%
	ofstream ofs1("testting_data.txt");
	for(int i=n-1; i>m-1; i--){

		ofs1<<arr[i].x<<"  "<<arr[i].y<<endl;
	}
	ofs.close();
		ifstream ifs1("testting_data.txt");
		for(int i=0;i<d;i++){
		 ifs1>>p[i].x>>p[i].y;
	}
	ifs1.close();
	cout<<"data testting:\n";
	    	for(int i=0;i<d;i++){
			cout<<" "<<p[i].x<<" "<<p[i].y<<" ? \n"<<endl;
	}
	cout<<"\nket qua : "<<endl;
	ofstream ofs2("output.txt");
    for(int i=0; i<d; i++){
   		ofs2<<"mau "<<i+1<<":	";
   		ofs2<<"x1"<<"="<<p[i].x<<"	x2"<<"="<<p[i].y;
   		phanLopKnn(arr, m, p[i]);
   		string Class = ketqua(arr, f);	
   		p[i].val=Class;
   		ofs2<<"\nket qua : "<<endl;
   		ofs2<<"x1"<<"= "<<p[i].x<<"	x2"<<"="<<p[i].y<<"	==> y= "<<Class<<endl<<endl;
   		cout<<"x1"<<"= "<<p[i].x<<"	x2"<<"="<<p[i].y<<"	==> y= "<<Class<<endl<<endl;
	}
	Confusion_Matrix(n,m,p,arr);
	
    return 0;
    
}
