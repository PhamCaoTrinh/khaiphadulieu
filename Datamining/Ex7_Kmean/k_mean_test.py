import numpy as np
from sklearn import cluster
from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
from k_mean import KMeans

X, y = make_blobs(centers=4, n_samples=8, n_features=2, shuffle=True, random_state=42)
print(X.shape)

clusters = 3
print(clusters)

k = KMeans(K=clusters, max_iters=150, plot_steps=False)
y_pred = k.predict(X)

k.plot()